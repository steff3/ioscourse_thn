//
//  BuddyDetailsViewController.swift
//  BuddyOrganiser
//
//  Created by steff3 on 19.12.18.
//  Copyright © 2018 steff3. All rights reserved.
//

import UIKit

class BuddyDetailsViewController: UIViewController {
    
    @IBOutlet var editButton: UIBarButtonItem?
    @IBOutlet var saveButton: UIBarButtonItem?
    @IBOutlet var cancelButton: UIBarButtonItem?

    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    
    var selectedBuddy: Buddy?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        generateBarButtons()
        configureBarButtons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        firstNameLabel.text = selectedBuddy?.firstName
        lastNameLabel.text = selectedBuddy?.lastName
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension BuddyDetailsViewController {
    func generateBarButtons() {
        editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(didTapEdit(_:)))
        
        saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(didTapSave(_:)))
        
        cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(didTapCancel(_:)))
        
    }
    
    func configureBarButtons() {
        if self.isEditing {
            self.navigationItem.rightBarButtonItem = saveButton
            self.navigationItem.leftBarButtonItem = cancelButton
        } else {
            self.navigationItem.rightBarButtonItem = editButton
            self.navigationItem.leftBarButtonItem = nil
        }
    }
    
    @objc func didTapEdit(_: UIBarButtonItem) {
        print("did tap edit")
        self.isEditing = true
        configureBarButtons()
    }
    
    @objc func didTapSave(_: UIBarButtonItem) {
        self.isEditing = false
        configureBarButtons()
    }
    
    @objc func didTapCancel(_: UIBarButtonItem) {
        self.isEditing = false
        configureBarButtons()

    }
}
