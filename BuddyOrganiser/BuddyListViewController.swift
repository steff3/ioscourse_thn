//
//  BuddyListViewController.swift
//  BuddyOrganiser
//
//  Created by steff3 on 19.12.18.
//  Copyright © 2018 steff3. All rights reserved.
//

import UIKit

class BuddyListViewController: UITableViewController {
    
    var source: [Buddy]?

    override func viewDidLoad() {
        super.viewDidLoad()

        source = [ Buddy(firstName: "Lisa", lastName: "Müller"),
        Buddy(firstName: "Peter", lastName: "Bauer") ]
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return source?.count ?? 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BuddyCell")!

        if let items = source {
            let item = items[indexPath.row]
            cell.textLabel?.text = item.firstName
        }

        return cell
    }


    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showBuddyDetails",
            let cell = sender as? UITableViewCell,
            let indexPath = self.tableView.indexPath(for: cell),
            let items = source,
            let vc = segue.destination as? BuddyDetailsViewController {
            
            let item = items[indexPath.row]
            vc.selectedBuddy = item
        }
        
    }
    

}
