//
//  Buddy.swift
//  BuddyOrganiser
//
//  Created by steff3 on 19.12.18.
//  Copyright © 2018 steff3. All rights reserved.
//

import Foundation


struct Buddy {
    let firstName: String
    let lastName: String
}
